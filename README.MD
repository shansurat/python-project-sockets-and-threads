# **CASE STUDY 1** | TCP Client Programming

## **Problem**

### In this problem you must program a simple TCP Service Scanner. Scanners are usually used to detect network services in the Local Area Network. You have to write a program that detects which services are available on a specific IP address range. For example, the machine 192.168.0.1 can hold a Web Server, an FTP server and a Mail Server; all these services are located on different Ports (80, 21, 25). The program must check a range of Ports and identify which service is available within a range of IP Addresses. After the application is finished, it has to write a report in a single text file, with the following format:

    IP Address
    Service 1 (Port)
    Service 2 (Port)
    IP Address
    Service 1 (Port)

### Unidentified services can be named as: Unknown Service. The application should receive 4 parameters:

### **Start IPAddress, End IPAddress, Start Port, End Port.**

## **Execution Example**

`shell`

```properties
python3 TCPServiceScanner 192.168.0.1 192.168.0.254 1 1024
```

```bash
Checking...192.168.0.1
3 services
Checking...192.168.0.2
1 service
...
...
```

`report.txt`

```
192.168.0.1
FTP Server (21)
Web Server (80)
SSH Server (22)
192.168.0.2
FTP Server (2)
...
...
End of File
```

# **CASE STUDY 2** | Concurrent Server Programming

## **Quiz Game**

### In this assignment you are asked to write a system to support an online math contest. The contest consists of answering the maximum quantity of sums in 30 seconds. This is done in a concurrent way, which means, that there are three participants answering at the same time. The participant with most correct answers wins.

### The jury is responsible in starting up the server, after that they wait for the clients. When the three clients are connected, they must send the message: READY to the server. When the server receives the three READY from the clients, the contest starts. At the end of the program, the server must show the results.

## **Example of Execution (SERVER)**

`shell`

```
python3 MathinikServer
```

```
192.168.1.2 Connected
192.168.1.2 is Anne
192.168.1.4 Connected
192.168.1.4 is Billy
192.168.1.3 Connected
192.168.1.3 is Mark
Mark is READY
Anne is READY
Billy is READY
Starting Contest...
Contest Finished
The results are:

1. Mark(7 correct / 2 wrong)
2. Anne (6 correct / 6 wrong)
3. Billy (4 correct / 7 wrong)
```

## **Example of Execution (CLIENT)**

`shell`

```
python3 MathinikClient 192.168.1.1
```

```
Please enter your name:

> > Billy
> > Write READY when you are prepared
> > READY
> > Contest Started!
> > Question 1: 25+23?
> > 48
> > ...
> > ...
> > Time is UP! The results are:

1. Mark(7 correct / 2 wrong)
2. Anne (6 correct / 6 wrong)
3. Billy (4 correct / 7 wrong)
```

## **NOTE**

### The numbers are 2 digits long. Use random generator in python with seed 99.

# **CASE STUDY 3** | Thread Programming

## **Quote, Unquote**

### In this assignment you are asked to write a system to support sending and receiving of quotes. The client program will connect to a quote server which in turn will send out a quote to that specific client.

### The quote server will read a file containing one-liner quotes stored on a text file that will be the basis for quotes sent to clients.

## **Example of Execution (SERVER)**

`shell`

```properties
python3 QuoteServer
```

```
Sending quotes to: /192.168.1.100
```

## **Example of Execution (CLIENT)**

`shell`

```properties
python3 QuoteClient 192.168.1.1
```

```
Quote of the moment: Life is Wonderful. Without it we’d all be dead.
```
