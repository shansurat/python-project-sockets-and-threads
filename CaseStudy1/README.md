# Case Study 1

## TCP Client Programming

### Apparently, there are two files in this directory, the [TCPServiceScanner](./TCPServiceScanner) and the [TCpServiceScanner_withThreader](./TCPServiceScanner_withThreader). Both scripts have the some functionality which is aligned to the specifications of Case Study 1. The difference is that the latter runs with a _threader_ so the port scanning process is asynchronous which makes it tremendously faster. The script is equipped with a _timer_ to compare results.

## Running the tests

### Both scripts have the same format of command-line arguments which is also described in the [Case Study Specifications](../SPECS.MD#case-study-1)
